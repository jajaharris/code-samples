﻿/// <reference path="Carvana.Cookie.js" />
/// <reference path="lib/moment.js" />

CV.AdClickTracking = (function (window, logger) {
    var constants = {
        GOOGLE_CLICK_ID: "gclid",
        GOOGLE_CLICK_SOURCE: "gclsrc",
        AD_WORDS: "aw",
        DAYS_TO_KEEP: 90
    }

    logger.loadLog("CV.AdClickTracking");

    var captureGoogleClickId = function () {
        try {
            var googleClickId = getQueryStringParameter(constants.GOOGLE_CLICK_ID);

            if (googleClickId) {
                var googleClickSource = getQueryStringParameter(constants.GOOGLE_CLICK_SOURCE);
                var hasAdWordsParameter = !googleClickSource || googleClickSource.indexOf(constants.AD_WORDS) !== -1;

                if (hasAdWordsParameter) {
                    var newEntry = generateAdClickEntry(googleClickId);
                    CV.Cookie.set(CV.Cookie.keys.GoogleClickId, newEntry, constants.DAYS_TO_KEEP);
                }
            }
        } catch (e) {
            logger.log("Error: " + e.message);
        } 
    };
    
    function generateAdClickEntry(googleClickId) {
        var previousEntry = CV.Cookie.get(CV.Cookie.keys.GoogleClickId) || "";
        var delimiter = previousEntry.length > 0 ? "|" : "";
        var newEntry = previousEntry + delimiter + googleClickId + "," + moment.utc().format();
	    return newEntry;
	}

    function getQueryStringParameter(paramName) {
        var match = RegExp('[?&]' + paramName + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    var updateAdClickElements = function() {
        try {
            var adClickEntries = CV.Cookie.get(CV.Cookie.keys.GoogleClickId) || "";
            var cookieHasAdClickEntries = adClickEntries.length > 0;
            var pageHasAdClickElements = $("#AdClickEntries").length && $("#AdType").length;

            if (cookieHasAdClickEntries && pageHasAdClickElements) {
                $("#AdClickEntries").val(adClickEntries);
                $("#AdType").val(CV.Cookie.keys.GoogleClickId);
                CV.Cookie.deleteCookie(CV.Cookie.keys.GoogleClickId);
            }
        } catch (e) {
            logger.log("Error: " + e.message);
        }
    };

    var saveAdClicksToDatabase = function() {
        try {
            var adClickEntries = CV.Cookie.get(CV.Cookie.keys.GoogleClickId) || "";

            if (adClickEntries.length > 0) {
                var apiParams = JSON.stringify({
                    adType: CV.Cookie.keys.GoogleClickId,
                    adClickEntries: adClickEntries
                });

                $.ajax({
                    url: "/account/SaveAdClicks",
                    type: 'POST',
                    contentType: 'application/json',
                    processData: false,
                    data: apiParams
                });

                CV.Cookie.deleteCookie(CV.Cookie.keys.GoogleClickId);
            }
        } catch (e) {
            logger.log("Error: " + e.message);
        }
    };	

    return {
        "captureGoogleClickId": captureGoogleClickId,
        "saveAdClicksToDatabase": saveAdClicksToDatabase,
        "updateAdClickElements": updateAdClickElements
    };
}(window, { loadLog: CV.loadLog, log: CV.log }));