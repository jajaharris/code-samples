﻿Admin.ManageDeliveryOptions = (function () {
    Admin.loadLog("Admin.ManageDeliveryOptions"); 

    var init = function (viewModel) {
        var constants = {
            SUCCESSFUL_WITH_DATA: "SuccessfulWithData",
            SUCCESSFUL_STATUS_ONLY: "SuccessfulStatusOnly",
            SUCCESSFUL_WITH_FAILURES: "SuccessfulWithFailures",
            SUCCESSFUL_NO_DATA_FOUND: "SuccessfulNoDataFound",
            EXCEPTION: "Exception",
            UNDEFINED: "undefined"
        };

        if (viewModel) {
            updateDeliveryTypeValues();
            configureDateRangeControl();
            initButtonClickHandlers();
            expandPurchaseBlockFailures();
        }

        //would not normally use magic numbers here but in this scenario number actually provides value
        function updateDeliveryTypeValues() {
            switch(viewModel.DeliveryType) {
                case 1:
                    viewModel.DeliveryTypeName = "Direct";
                    break;

                case 2:
                    viewModel.DeliveryTypeName = "Hub";
                    break;

                case 3:
                    viewModel.DeliveryTypeName = "Local";
                    break;

                case 4:
                    viewModel.DeliveryTypeName = "Regional";
                    break;

                default:
                    viewModel.DeliveryTypeName = "Local";
            }
        }

        function configureDateRangeControl() {
            var minStartDate = new Date();

            $('#dateRangeInput').dateRangePicker(
            {
                inline: true,
                container: '#dateRangeContainer',
                alwaysOpen: true,
                startDate: minStartDate,
                setValue: onDateRangeSelected
            });
        }

        function onDateRangeSelected(s, selectedStartDate, selectedEndDate) {
            $('#startDate').val(selectedStartDate);
            $('#endDate').val(selectedEndDate);
        }

        function initButtonClickHandlers() {
            $('#btnShowCalendarsModal').on('click', showCalendarsModal);
            $('#btnExecuteBulkBlock').on('click', executeBulkBlock);
            $('#btnCloseFailuresModal').on('click', closeFailuresModal);

            $('#btnGetDatesToUnblock').on('click', getDatesToUnblock);
            $('#btnExecuteBulkUnblock').on('click', executeBulkUnblock);
        }

        function refreshPage() {
            var deliveryScheduleUrl = '/admin/deliveryschedule?deliverytype=' + viewModel.DeliveryTypeName + '&locationid=' + viewModel.Location.LocationId;
            window.location.replace(deliveryScheduleUrl);
        }

        function expandPurchaseBlockFailures() {
            var nothingInStorage = sessionStorage.length === 0 || sessionStorage["purchaseBlocks"] === undefined;

            if (nothingInStorage) {
                return;
            }

            var purchaseTimeSlots = JSON.parse(sessionStorage["purchaseBlocks"]);
            var expandedCount = 0;

            $('#timeSlotsTable tr').each(function (rowIndex) {
                var $currentRow = $(this);
                var deliveryDate = $currentRow.find('td').eq(1).text();

                if (deliveryDate !== "") {
                    var startTime = $currentRow.find('td').eq(2).text();
                    var tableRowTimeSlot = deliveryDate + ' ' + startTime;
                    var canExpandRow = purchaseTimeSlots.indexOf(tableRowTimeSlot) > -1;
                    
                    if (canExpandRow) {
                        var deliveryIndex = $currentRow.find('td').eq(0).attr("data-deliveryindex");
                        var bay = $currentRow.find('td').eq(0).attr("data-bay");

                        GetScheduleSlotDetails(deliveryDate,
                                               startTime,
                                               viewModel.Location.LocationId,
                                               viewModel.DeliveryTypeName,
                                               bay,
                                               viewModel.TimeZoneId,
                                               deliveryIndex);

                        ++expandedCount;
                    }                    
                }

                if (expandedCount === purchaseTimeSlots.length) {
                    sessionStorage.clear();
                    return false;
                }
            });            
        }       

        function closeFailuresModal(e) {
            e.preventDefault();
            var $modal = $('#bulkBlockFailuresModal');
            $modal.modal('hide');
            refreshPage();
        }

        function validateBulkBlockData(startDate, comment) {
            var message = "";

            if (startDate.length === 0) {
                message = "Please select a start date";
            } else if (comment === "") {
                message = "Please enter a comment describing the reason for blocking this time";
            } else if (comment.length > 140) {
                message = "Comment must be less than 140 characters";
            }

            if (message.length === 0) {
                return true;
            }

            var $modal = $('#calendarsModal');
            var $errLabel = $modal.find('#bulk-block-error-message');
            $errLabel.text(message);
            $errLabel.show();
            return false;                        
        }

        function executeBulkBlock(e) {
            var $modal = $('#calendarsModal');
            $modal.find('#bulk-block-error-message').hide();
            var comment = $('#bulkBlockComment').val();
            var startDate = $modal.find('#startDate').val();
            var endDate = $modal.find('#endDate').val();            

            var isValid = validateBulkBlockData(startDate, comment);

            if (isValid) {
                var isSingleDayBlock = endDate === "";

                if (isSingleDayBlock) {
                    endDate = startDate;
                }

                var apiParams = JSON.stringify({
                    deliveryType: viewModel.DeliveryTypeName,
                    locationId: viewModel.Location.LocationId,
                    timeZoneId: viewModel.TimeZoneId,
                    startDate: startDate,
                    endDate: endDate,
                    bulkBlockComment: comment
                });

                $.ajax({
                    url: "/admin/BulkBlockDeliverySlots",
                    type: 'POST',
                    contentType: 'application/json',
                    processData: false,
                    data: apiParams,
                    success: onBulkBlockCompleted
                });
            }           

            return false;
        }

        function onBulkBlockCompleted(svcResponse) {
            var $modal = $('#calendarsModal');

            $modal.modal('hide');

            switch (svcResponse.Status) {
                case constants.SUCCESSFUL_STATUS_ONLY:
                    refreshPage();
                    break;

                case constants.SUCCESSFUL_WITH_FAILURES:
                    saveFailuresToStorage(svcResponse.Items);
                    showFailuresModal(svcResponse.Items);
                    break;
            }
        }

        function saveFailuresToStorage(purchaseBlocks) {
            //typeof always returns a string which is why undefined should be in quotes and evaluated as a string
            if (typeof (Storage) !== constants.UNDEFINED) {
                var storagePurchases = [];

                for (var i = 0; i < purchaseBlocks.length; i++) {
                    var purchaseObj = purchaseBlocks[i];
                    var startTime = purchaseObj.StartTime.replace(/\s+/g, '');
                    var timeSlot = purchaseObj.DeliveryDate + ' ' + startTime;
                    var timeSlotFound = storagePurchases.indexOf(timeSlot) !== -1;

                    if (!timeSlotFound) {
                        storagePurchases.push(timeSlot);
                    }
                }

                sessionStorage["purchaseBlocks"] = JSON.stringify(storagePurchases);
            }
        }

        function showFailuresModal(purchases) {
            var $modal = $('#bulkBlockFailuresModal');
            var $table = $modal.find('#failuresTable');
            $('#failuresTable tr').slice(2).remove();
            var $newRow = $table.find('#failuresRowTemplate');
            $newRow.find('td').eq(0).text("");
            $newRow.find('td').eq(1).text("");
            $newRow.find('td').eq(2).text("");
            $newRow.find('td').eq(3).text("");

            for (var i = 0; i < purchases.length; i++) {
                if (i > 0) {
                    $newRow = $newRow.clone();
                }

                $newRow.find('td').eq(0).text(purchases[i].DeliveryDate);
                $newRow.find('td').eq(1).text(purchases[i].StartTime);
                $newRow.find('td').eq(2).text(purchases[i].EndTime);
                $newRow.find('td').eq(3).text(purchases[i].PurchaseId);
                $newRow.appendTo($table);
            }

            $modal.modal('show');
        }

        function showCalendarsModal(e) {
            var $modal = $('#calendarsModal');
            $modal.find('#bulkBlockComment').val("");
            $modal.find('#dateRangeInput').data('dateRangePicker').clear();
            $modal.find('#bulk-block-error-message').hide();
            $modal.modal('show');
        }        

        function executeBulkUnblock(e) {
            e.preventDefault();
            var datesToUnblock = [];

            $('#bulkUnblockTable tr').filter(':has(:checkbox:checked)').find('#blocked-date').each(function () {
                datesToUnblock.push(this.innerText);
            });

            if (datesToUnblock.length > 0) {

                var apiParams = JSON.stringify({
                    deliveryType: viewModel.DeliveryTypeName,
                    locationId: viewModel.Location.LocationId,
                    datesToUnblock: datesToUnblock
                });

                $.ajax({
                    url: '/admin/BulkUnblockDeliverySlots',
                    contentType: 'application/json',
                    type: 'POST',
                    data: apiParams,
                    dataType: 'json',
                    processData: false,
                    success: onBulkUnblockCompleted
                });
            }
        }        

        function onBulkUnblockCompleted(svcResponse) {
            var $modal = $('#bulkUnblockModal');            

            switch (svcResponse.Status) {
                case constants.SUCCESSFUL_STATUS_ONLY:
                    $modal.modal('toggle');
                    refreshPage();
                    break;

                case constants.EXCEPTION:
                    var $errLabel = $modal.find('#bulk-unblock-error-message');
                    $errLabel.text("An unexpected error has occurred - please try again");
                    $errLabel.show();
                    break;
            }
        }

        function getDatesToUnblock(e) {
            var apiParams = JSON.stringify({
                deliveryType: viewModel.DeliveryTypeName,
                locationId: viewModel.Location.LocationId
            });

            $.ajax({
                url: '/admin/GetDatesToUnblock',
                contentType: 'application/json',
                type: 'POST',
                processData: false,
                data: apiParams,
                dataType: 'json',
                success: onGetDatesToUnblockCompleted
            });
        }        

        function onGetDatesToUnblockCompleted(svcResponse) {            
            switch (svcResponse.Status) {
                case constants.SUCCESSFUL_NO_DATA_FOUND:
                    alert("There are no blocks scheduled at this time");
                    break;

                case constants.SUCCESSFUL_WITH_DATA:
                    showBulkUnblockModal(svcResponse.Items);
                    break;

                case constants.EXCEPTION:
                    alert("An unexpected error has occurred - please try again");
                    break;
            }
        }

        function showBulkUnblockModal(datesToUnblock) {
            var $modal = $('#bulkUnblockModal');
            var $table = $modal.find('#bulkUnblockTable');
            $('#bulkUnblockTable tr').slice(2).remove();
            var $newRow = $table.find('#bulkUnblockRowTemplate');
            $newRow.find('td').eq(1).text("");
            $newRow.find('td').eq(2).text("");

            for (var i = 0; i < datesToUnblock.length; i++) {
                if (i > 0) {
                    $newRow = $newRow.clone();
                }

                $newRow.find('td').eq(1).text(datesToUnblock[i].BlockedDate);
                $newRow.find('td').eq(2).text(datesToUnblock[i].Count);
                $newRow.appendTo($table);
            }

            $modal.modal('show');
        }

    return {
        "init": init
    };
}());