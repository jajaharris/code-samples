﻿/// <reference path="~/Scripts/lib/JSLINQ.js" />

CV.AddressValidation = (function ($, logger) {
    logger.loadLog("CV.AddressValidation");
    var params = {};
    var validationComplete = false;
    var validationTypes = { DeliveryAddress: "DeliveryAddress", PostalAddress: "PostalAddress" };
    var dialogTypes = { InvalidAddress: "InvalidAddress", AddressNotRecognized: "AddressNotRecognized" };
    var currentValidationType = validationTypes.DeliveryAddress;

    var dialogs = function() {
        return dialogTypes;
    };

    var validationCompleted = function () {
        return validationComplete;
    };

    var addressValidationTypes = function () {
        return validationTypes;
    };

    var validateAddress = function (validationParams) {
        params = validationParams;
        var addressToValidate = currentValidationType === validationTypes.DeliveryAddress
                                ? params.DeliveryAddress
                                : params.PostalAddress;

        var addressIsInvalid = hasMissingAddressComponents(addressToValidate);

        if (addressIsInvalid) {
            var displayPoBoxDialog = false;
            processFailedAddress(displayPoBoxDialog);
        } else {
            $.ajax("/account/addressverify", {
                type: "POST",
                data: JSON.stringify(addressToValidate),
                success: onValidateAddressCompleted
            });
        }
    };

    function hasMissingAddressComponents(addressToValidate) {
        return addressToValidate === undefined ||
                addressToValidate === null ||
                (
                    (addressToValidate.address1() === undefined || addressToValidate.address1() === "") &&
                    (addressToValidate.address2() === undefined || addressToValidate.address2() === "")
                ) ||
                (addressToValidate.city() === undefined || addressToValidate.city() === "") ||
                (addressToValidate.state() === undefined || addressToValidate.state() === "") ||
                (addressToValidate.zipCode() === undefined || addressToValidate.zipCode() === "");
    }

    function onValidateAddressCompleted(verifiedAddresses) {
        logger.log("Successful callback from /account/addressverify ajax request");
        logger.log(verifiedAddresses);

        var validationResults = isValidAddress(verifiedAddresses);

        if (validationResults.isValid) {
            processVerifiedAddress(verifiedAddresses);
        } else {
            processFailedAddress(validationResults.isPoBoxFailure);
        }
    }

    function isValidAddress(verifiedAddresses) {
        var addressCount = verifiedAddresses.length;

        if (addressCount === 0) {
            return {
                isValid: false,
                isPoBoxFailure: false
            };
        }

        switch (currentValidationType) {
            case validationTypes.DeliveryAddress:
                var hasEligibleDeliveryAddress = containsAnEligibleDeliveryAddress(verifiedAddresses);
                var poBoxWasFound = containsPoBox(verifiedAddresses);
                var onlyAddressFoundIsPoBox = (addressCount === 1) && poBoxWasFound;
                var allAddressesArePoBoxes = containsAllPoBoxes(verifiedAddresses);

                return {
                    isValid: hasEligibleDeliveryAddress,
                    isPoBoxFailure: onlyAddressFoundIsPoBox || allAddressesArePoBoxes
                };

            case validationTypes.PostalAddress:
                return {
                    isValid: true,
                    isPoBoxFailure: false
                };
        }
    }

    function containsAllPoBoxes(verifiedAddresses) {
        var onlyPoBoxes = JSLINQ(verifiedAddresses).All(function (address) {
            return address.IsPostOfficeBox !== undefined && address.IsPostOfficeBox !== null && address.IsPostOfficeBox;
        });

        return onlyPoBoxes;
    }

    function containsAnEligibleDeliveryAddress(verifiedAddresses) {
        var hasEligibleAddress = JSLINQ(verifiedAddresses).Any(function (address) {
            return address.IsPostOfficeBox !== undefined && address.IsPostOfficeBox !== null && !address.IsPostOfficeBox;
        });

        return hasEligibleAddress;
    }

    function containsPoBox(verifiedAddresses) {
        var foundPoBox = JSLINQ(verifiedAddresses).Any(function (address) {
            return address.IsPostOfficeBox !== undefined && address.IsPostOfficeBox !== null && address.IsPostOfficeBox;
        });

        return foundPoBox;
    }

    function processFailedAddress(isPoBoxFailure) {
        var message = isPoBoxFailure
            ? "Attempted to use a PO Box for the delivery address, erroring"
            : "The " + currentValidationType + " is invalid";

        logger.log(message);
        var elementID = (currentValidationType === validationTypes.DeliveryAddress)
                ? params.DeliveryAddressElementID
                : params.PostalAddressElementID;

        currentValidationType = validationTypes.DeliveryAddress;

        switch(params.InvalidDialog) {
            case dialogTypes.AddressNotRecognized:
                if (isPoBoxFailure) {
                    CV.SmartyStreets.openAddressInvalidDialog("right", elementID, isPoBoxFailure);
                } else {
                    CV.SmartyStreets.openNotRecognizedDialog("right", elementID);
                }
                
                return;

            case dialogTypes.InvalidAddress:
                CV.SmartyStreets.openAddressInvalidDialog("right", elementID, isPoBoxFailure);
                return;
        }        
    }

    function processVerifiedAddress(verifiedAddresses) {
        populateViewModelWithVerifiedAddresses(verifiedAddresses);
        var eligibleAddressCount = params.AddressList().length;

        if (eligibleAddressCount > 1) {
            logger.log("showing address list dialog");
            CV.SmartyStreets.openAddressList("right");
        } else {
            logger.log("found a verified address, updating the appropriate address");
            var addressToUpdate = (currentValidationType === validationTypes.DeliveryAddress)
                ? params.DeliveryAddress
                : params.PostalAddress;

            addressToUpdate.update(params.AddressList()[0]);

            switch (currentValidationType) {
                case validationTypes.DeliveryAddress:
                    if (params.HasPostalAddress) {
                        currentValidationType = validationTypes.PostalAddress;
                        validateAddress(params);
                    } else {
                        validationComplete = true;
                        executeValidationSuccessfulCallback();
                    }

                    break;

                case validationTypes.PostalAddress:
                    validationComplete = true;
                    executeValidationSuccessfulCallback();
                    break;
            }
        }
    }

    function executeValidationSuccessfulCallback() {
        if (params.ValidationSuccessfulCallback !== undefined && params.ValidationSuccessfulCallback !== null) {
            params.ValidationSuccessfulCallback();
        }
    }

    function populateViewModelWithVerifiedAddresses(verifiedAddresses) {
        params.AddressList([]);

        for(var i = 0; i < verifiedAddresses.length; i++) {
            var verifiedAddress = verifiedAddresses[i];

            if (verifiedAddress !== undefined && verifiedAddress !== null) {
                var isPostalAddressValidation = (currentValidationType === validationTypes.PostalAddress);
                var isEligibleDeliveryAddress = !verifiedAddress.IsPostOfficeBox;

                if (isPostalAddressValidation || isEligibleDeliveryAddress) {
                    var address = new CV.Finance.viewModel.Address(verifiedAddress);
                    address.verified(true);
                    params.AddressList.push(address);
                }
            }
        }
    }    

    return {
        "validateAddress": validateAddress,
        "addressValidationTypes": addressValidationTypes,
        "validationCompleted": validationCompleted,
        "dialogs": dialogs
    };
})(jQuery, { loadLog: CV.loadLog, log: CV.log });