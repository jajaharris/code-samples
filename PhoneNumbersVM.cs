using APETS.Data;
using APETS.Models.Groups.Address.AddressScreen;
using APETS.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace APETS.ViewModels.Groups.Address
{
    public class PhoneNumbersVM : BaseVM
    {
        private long clientRsn;
        private long sourceRsn;
        public PhoneSourceTables SourceTable;
        private PhoneLockState lockState;

        public readonly string CONFIRM_DELETE = "Are you sure you want to delete this row?";
        public readonly string CONFIRM_DELETE_TITLE = "Confirm Delete";
        public const long TREEVIEW_FUNCTION_RSN = 0;
        public IEnumerable<Phone> Phones { get; private set; }
        public IEnumerable<RPhoneType> PhoneTypeItems { get; private set; }
        public readonly IEnumerable<CodeItem> PrimaryItems = RootUtils.CreateCodeItemsFor<YesNo>();
        public readonly IEnumerable<CodeItem> ValidatedItems = RootUtils.CreateCodeItemsFor<YesNo>();
        public readonly IEnumerable<CodeItem> ActiveItems = RootUtils.CreateCodeItemsFor<Active>();

        public PhoneNumbersVM(long clientRsn, PhoneSourceTables sourceTable, long sourceRsn, PhoneLockState lockState)
            : base(TREEVIEW_FUNCTION_RSN, RootConstants.DEFAULT_CLIENT_SECURITY)
        {
            this.clientRsn = clientRsn;
            this.SourceTable = sourceTable;
            this.sourceRsn = sourceRsn;
            this.lockState = lockState;
        }

        public void LoadData()
        {
            Phones = Services.AddressDataService.GetPhonesScreenData(clientRsn, sourceRsn, SourceTable);
            Phones.CaptureOriginalValues();
            PhoneTypeItems = Services.RTablesService.PhoneTypes;
        }

        public override bool CanEdit(long addedByUserID = 0)
        {
            bool hasSecurityClearanceToEdit = base.CanEdit(addedByUserID);
            return hasSecurityClearanceToEdit &&
                   SourceTable != PhoneSourceTables.Demographics &&
                   lockState == PhoneLockState.Unlock &&
                   Client.IsAssigned;
        }

        public override bool CanDelete(long addedByUserID = 0, SpecialOpenCodes specialOpen = SpecialOpenCodes.None, long itemRsn = 0)
        {
            bool hasSecurityClearanceToDelete = base.CanDelete(addedByUserID, specialOpen);

            if (!hasSecurityClearanceToDelete)
            {
                return false;
            }

            if (SourceTable == PhoneSourceTables.Demographics || !Client.IsAssigned)
            {
                return false;
            }

            Phone phoneToDelete = Phones.Single(x => x.phones_rsn == itemRsn);

            if (SourceTable == PhoneSourceTables.Employee && phoneToDelete.IsPrimary)
            {
                return false;
            }

            return true;
        }

        protected override void InitializeCanAdd()
        {
            base.InitializeCanAdd();
            bool hasSecurityClearanceToAdd = CanAdd;
            CanAdd = CurrentUser.IsDeveloperOrBusinessAnalyst || (hasSecurityClearanceToAdd && Client.IsAssigned && SourceTable != PhoneSourceTables.Demographics);
        }

        public void DeletePhone(Phone deletedPhone, PhoneSourceTables sourceTable)
        {
            Services.AddressDataService.DeletePhone(deletedPhone, SourceTable);
        }

        public long SavePhone(bool isInsert, Phone phone)
        {
            ValidatePhone(isInsert, phone);
            Phone previousPrimaryPhone = ValidateUniquePrimaryType(phone);
            DateTime serverTime = ServerTime;

            if (isInsert)
            {
                phone.source_table = SourceTable.GetDescription();
                phone.source_rsn_id = sourceRsn;
                phone.add_date = serverTime;
                phone.add_userid = CurrentUser.employee_rsn;
                phone.client_rsn = clientRsn;
            }

            phone.mod_date = serverTime;
            phone.mod_userid = CurrentUser.employee_rsn;
            long rsnOfSavedPhone = Services.AddressDataService.SavePhone(isInsert, phone);

            bool isWorkOrFaxNumber = phone.PhoneType == PhoneTypes.Work || phone.PhoneType == PhoneTypes.Fax;

            if (SourceTable == PhoneSourceTables.Employee && isWorkOrFaxNumber)
            {
                Services.AddressDataService.UpdateEmployeeOfficeOrFaxPhone(phone);
            }

            if (previousPrimaryPhone != null)
            {
                Services.AddressDataService.DemotePrimaryPhone(previousPrimaryPhone.phones_rsn);
            }

            return rsnOfSavedPhone;
        }

        private Phone ValidateUniquePrimaryType(Phone phone)
        {
            if (!phone.IsPrimary)
            {
                return null;
            }

            Phone phoneToDemote = Phones.FirstOrDefault(x => x.phones_rsn != phone.phones_rsn && x.IsPrimary && x.PhoneType == phone.PhoneType);

            if (phoneToDemote != null)
            {
                const string PRIMARY_EXISTS_MSG = "Another phone number of this type has already been marked as primary.  Would you like to remove it?";
                const string PRIMARY_EXISTS_TITLE = "Primary Already Exists";

                ConfirmationResponse response = ShowYesNoMsgBox(PRIMARY_EXISTS_MSG, PRIMARY_EXISTS_TITLE);

                if (response == ConfirmationResponse.No)
                {
                    phone.primary_number = 0;
                    return null;
                }
                else if (response == ConfirmationResponse.Yes)
                {
                    return phoneToDemote;
                }
            }

            return null;
        }

        public void AllowAddNewValidations()
        {
            if (!Client.IsAssigned)
            {
                throw new ApetsException(new Failure(PhoneReasons.ClosedRecord));
            }
        }

        private void ValidatePhone(bool isInsert, Phone phone)
        {
            var failures = new List<Failure>();

            if (phone.phone_number.IsNullOrEmpty())
            {
                failures.Add(new Failure(DataEntryReasons.RequiredFieldMissing, nameof(phone.phone_number)));
            }
            else if (phone.phone_number.RemoveNumberFormat().Length < 10)
            {
                failures.Add(new Failure(DataEntryReasons.MinLengthExceeded, nameof(phone.phone_number), "Phone", "10"));
            }
            else if (phone.phone_number.HasRepeatedCharacters())
            {
                string repeatedValue = phone.phone_number.Substring(0, 1);
                failures.Add(new Failure(DataEntryReasons.HasRepeatedCharacters, nameof(phone.phone_number), repeatedValue));
            }

            var MINIMUM_EFFECTIVE_DATE = new DateTime(1960, 1, 1);

            if (phone.effective_date == DateTime.MinValue)
            {
                failures.Add(new Failure(DataEntryReasons.RequiredFieldMissing, nameof(phone.effective_date)));
            }
            else if (phone.effective_date > DateTime.Today)
            {
                failures.Add(new Failure(DataEntryReasons.MaxDateExceeded, nameof(phone.effective_date)));
            }
            else if (phone.effective_date < MINIMUM_EFFECTIVE_DATE)
            {
                failures.Add(new Failure(DataEntryReasons.MinDateExceeded,
                                         nameof(phone.effective_date),
                                         "Effective Date",
                                         MINIMUM_EFFECTIVE_DATE.ToSafeShortDateString()));
            }

            if (phone.phone_types_rsn <= 0)
            {
                failures.Add(new Failure(DataEntryReasons.RequiredFieldMissing, nameof(phone.phone_types_rsn)));
            }

            if (phone.active_cd.IsNullOrEmpty())
            {
                failures.Add(new Failure(DataEntryReasons.RequiredFieldMissing, nameof(phone.active_cd)));
            }

            if (phone.primary_number == RootConstants.INVALID_SHORT_VALUE)
            {
                failures.Add(new Failure(DataEntryReasons.RequiredFieldMissing, nameof(phone.primary_number)));
            }
            else if (phone.IsPrimary && phone.IsInactive)
            {
                failures.Add(new Failure(PhoneReasons.PrimaryCantBeInactive));
            }
            else
            {
                bool isUpdate = !isInsert;
                bool isWorkPhoneNumber = phone.PhoneType == PhoneTypes.Work;

                if (isUpdate && isWorkPhoneNumber && SourceTable == PhoneSourceTables.Employee)
                {
                    Phone originalPhone = Phones.Single(x => x.phones_rsn == phone.phones_rsn);

                    if (originalPhone.IsPrimary && !phone.IsPrimary)
                    {
                        failures.Add(new Failure(PhoneReasons.CantDemotePrimaryWorkNumber));
                    }
                }
            }

            if (failures.Any())
            {
                throw new ApetsException(failures);
            }
        }
    }

    public enum PhoneReasons
    {
        [Description("A record marked as primary can not be inactive")]
        PrimaryCantBeInactive = 1,

        [Description("A primary work number can only be made non-primary by setting another work number to primary")]
        CantDemotePrimaryWorkNumber,

        [Description("Closed Record|The parent record is closed.  No additional records may be added")]
        ClosedRecord
    }
}
