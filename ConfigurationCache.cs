using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using USADSI.ImpactDocumentManagement.Services.Configuration;
using USADSI.ImpactDocumentManagement.Services.DataContracts;
 
namespace USADSI.ImpactDocumentManagement.Services.Bridge
{
    public class ConfigurationCache
    {
        private Dictionary<ExternalServiceTypes, IExternalServiceConfiguration> _configs;
        private DocumentManagementService _impactSvc;
        private string _impactDatabaseServer;
        private string _impactDatabase;
        private string _impactUserInitials;
 
        public ExternalServiceTypes DefaultSystem { get; private set; }
 
        public ConfigurationCache(DocumentManagementService impactSvc, string impactDbServer, string impactDbName, string impactUserInitials)
        {
            _impactSvc = impactSvc;
            _impactDatabaseServer = impactDbServer;
            _impactDatabase = impactDbName;
            _impactUserInitials = impactUserInitials;
 
            _configs = new Dictionary<ExternalServiceTypes, IExternalServiceConfiguration>();
            DefaultSystem = ExternalServiceTypes.None;
        }
 
        public IExternalServiceConfiguration this[ExternalServiceTypes system]
        {
            get
            {
                return GetConfiguration(system);
            }
 
            set
            {
                if (_configs.ContainsKey(system))
                    _configs[system] = value;
                else
                    _configs.Add(system, value);
            }
        }
 
        private IExternalServiceConfiguration GetConfiguration(ExternalServiceTypes requestedSystem)
        {
            IExternalServiceConfiguration config = null;//returns null if not found after call to Load
 
            if (_configs.ContainsKey(requestedSystem))
                config = _configs[requestedSystem];
            else
            {
                Load(requestedSystem);
 
                if (_configs.ContainsKey(requestedSystem))
                    config = _configs[requestedSystem];
            }
 
            return config;
        }
 
        public void Load(ExternalServiceTypes requestedSystem)
        {
            List<ExternalServiceTypes> systems = new List<ExternalServiceTypes>() { requestedSystem };
            Load(systems);
        }
 
        public void Load(List<ExternalServiceTypes> requestedSystems)
        {
            GetExternalServicesConfigurationSettingsRequest request = new GetExternalServicesConfigurationSettingsRequest();
            request.ExternalServicesToRetrieveConfigurationFor = requestedSystems.ToArray();
            request.DatabaseServerName = _impactDatabaseServer;
            request.DatabaseName = _impactDatabase;
            request.UserInitials = _impactUserInitials;
 
            GetExternalServicesConfigurationSettingsResponse response = _impactSvc.GetExternalServicesConfigurationSettings(request);
 
            if (response.Success)
            {
                foreach (ExternalConfigurationSettings config in response.ConfigurationSettings)
                {
                    //convert from ConfigurationType to ExternalServiceTypes
                    ExternalServiceTypes systemType = ConvertEnum(config.ExternalServiceConfigurationType);
 
                    if (_configs.ContainsKey(systemType))
                        _configs[systemType] = config.ConfigurationSettings;
                    else
                        _configs.Add(systemType, config.ConfigurationSettings);
 
                    if ((DefaultSystem == ExternalServiceTypes.None) && config.ConfigurationSettings.IsDefaultPublisher)
                        DefaultSystem = systemType;
                }
            }
        }
 
        private ExternalServiceTypes ConvertEnum(ConfigurationTypes configType)
        {
            return (ExternalServiceTypes) Enum.Parse(typeof(ExternalServiceTypes), configType.ToString());
        }
    }
}



